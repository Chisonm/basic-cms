<nav class="sidebar">
  <div class="sidebar-header">
    <a href="#" class="sidebar-brand">
      cm<span>s</span>
    </a>
    <div class="sidebar-toggler not-active">
      <span></span>
      <span></span>
      <span></span>
    </div>
  </div>
  <div class="sidebar-body">
    <ul class="nav">
      <li class="nav-item nav-category">Main</li>
      <li class="nav-item">
        <a href="{{ url('admin/dashboard') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Dashboard</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Home</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('aboutus') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">About Us</span>
        </a>
      </li>
      <li class="nav-item">
        <a href="{{ url('/services') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Services</span>
        </a>
      </li>
      <li class="nav-item">
      
        <a href="{{ url('') }}" class="nav-link">
          <i class="link-icon" data-feather="box"></i>
          <span class="link-title">Contact Us</span>
        </a>
      </li>
    </ul>
  </div>
</nav>
