@extends('admin.layout.master')
@push('plugin-styles')
    <link rel="stylesheet" href="{{ asset('/assets/plugins/simplemde/simplemde.min.css') }}">
@endpush

@section('content')
<nav class="page-breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/aboutus">About Us</a></li>
    <li class="breadcrumb-item active" aria-current="page">Add contents</li>
  </ol>
</nav>

<div class="row">
  <div class="col-md-12 grid-margin stretch-card">
    <div class="card">
      <div class="card-body">
        <h4 class="card-title">Create contents</h4>
        {{-- @if($errors->any())
            <ul class="list-group ">
                @foreach ($errors->all() as $error)
                    <li class="list-group-item alert-fill-danger mb-2" role="alert">
                        {{ $error }}
                    </li>
                @endforeach
            </ul>
         @endif --}}
        <form action="{{ route('aboutus.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label>Title</label>
                <input type="text"  class="form-control @error('title') is-invalid @enderror" name="title" autocomplete="off" placeholder="Title" value="{{ old('title') }}">
                @error('title')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>
              <div class="form-group">
              <label>Sub Title</label>
              <input type="text" class="form-control @error('subtitle') is-invalid @enderror"  name="subtitle" autocomplete="off" placeholder="Sub Title" value="{{ old('subtitle') }}">
              @error('subtitle')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <textarea class="form-control @error('content') is-invalid @enderror" name="content" id="tinymceExample" rows="10" >
                {{ old('content') }}
                @error('content')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
              @enderror
            </textarea>
            <div class="form-group">
                <label>Image</label>
                <input type="file" class="form-control @error('image') is-invalid @enderror" name="image" autocomplete="off" value="{{ old('image') }}">
                @error('image')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
              </div>

              <button class="btn btn-primary">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>
    
@endsection
@push('plugin-scripts')
<script src="{{ asset('/assets/plugins/tinymce/tinymce.min.js') }}"></script>
@endpush

@push('custom-scripts')
<script src="{{ asset('/assets/js/tinymce.js') }}"></script>
@endpush