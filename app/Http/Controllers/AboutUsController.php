<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\AboutUs;

class AboutUsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = AboutUs::orderby('created_at','desc')->get();
        return view('admin.about-us.index',compact('abouts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.about-us.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'subtitle' => 'required',
            'content' => 'required',
            'image' => 'required|image'
        ]);

        // dd($request->all());
        $post = new AboutUs;

        $post_image = $request->image;
        $post_image_new_name = time() . $post_image->getClientOriginalName();
        $post_image->move('uploads/about-us', $post_image_new_name);

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->content = $request->content;
        $post->image = 'uploads/about-us/' . $post_image_new_name;

        $post->save();
        toastr()->success('Data has been saved successfully!');
        return redirect()->back();
        // 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = AboutUs::findOrFail($id);
        return view('admin.about-us.edit',compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[

        ]);
            $post = AboutUs::findOrFail($id);

        if($request->hasFile('image'))
        {
            $post_image = $request->image;

            $post_image_new_name = time() . $post_image->getClientOriginalName();

            $post_image->move('uploads/about-us', $post_image_new_name);

            $post->image = 'uploads/about-us/' . $post_image_new_name;

            $post->save();
        }

        $post->title = $request->title;
        $post->subtitle = $request->subtitle;
        $post->content = $request->content;

        $post->save();
        toastr()->success('Data has been Updated successfully!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $about = AboutUs::findOrFail($id);

        if(file_exists($about->image))
        {
            unlink($about->image);
        }

        $about->delete();

        toastr()->success('Data has been Deleted successfully!');
        return redirect()->back();
    }
}
